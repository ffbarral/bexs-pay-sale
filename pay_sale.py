import gspread
from oauth2client.service_account import ServiceAccountCredentials
from google.oauth2 import service_account
import base64
import io
import avro.io
from avro.datafile import DataFileWriter
import os
import gcloud
from gcloud import storage
from google.cloud import bigquery
from datetime import datetime, timedelta

key_path = 'cert/key.json'

credentials = service_account.Credentials.from_service_account_file(
    key_path,
    scopes=["https://www.googleapis.com/auth/cloud-platform",
         "https://spreadsheets.google.com/feeds",
         'https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive.file",
         "https://www.googleapis.com/auth/drive",
         "https://www.googleapis.com/auth/urlshortener",
         "https://www.googleapis.com/auth/sqlservice.admin",
         "https://www.googleapis.com/auth/cloud-platform",
         "https://www.googleapis.com/auth/compute",
         "https://www.googleapis.com/auth/devstorage.full_control",
         "https://www.googleapis.com/auth/logging.admin",
         "https://www.googleapis.com/auth/logging.write",
         "https://www.googleapis.com/auth/monitoring",
         "https://www.googleapis.com/auth/servicecontrol",
         "https://www.googleapis.com/auth/service.management.readonly",
         "https://www.googleapis.com/auth/bigquery",
         "https://www.googleapis.com/auth/datastore",
         "https://www.googleapis.com/auth/taskqueue",
         "https://www.googleapis.com/auth/userinfo.email",
         "https://www.googleapis.com/auth/trace.append",
         "https://www.googleapis.com/auth/plus.login",
         "https://www.googleapis.com/auth/plus.me",
         "https://www.googleapis.com/auth/userinfo.email",
         "https://www.googleapis.com/auth/userinfo.profile"],
)

client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id,
)

folder = str((datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d'))
data_folder = str((datetime.now() - timedelta(days=1)).strftime('%Y%m%d'))
bucket_name = 'gs://bexspay_prd/' + folder + '/payments/*.csv'
dataset = 'bq_bexs_sandbox'
tabela = 'pay_payments_prep'

new_file = 'cert/sale_register_' + data_folder + '.avro'
file_schema = 'cert/sale.avsc'
new_filename = 'sale_register_' + data_folder + '.avro'


# As file at filePath is deleted now, so we should check if file exists or not not before deleting them
if os.path.exists(new_file):
    os.remove(new_file)
    print("Delete file", new_file)
else:
    print("Can not delete the file as it doesn't exists")

bq1 = bigquery.Client(credentials=credentials, project=credentials.project_id)
#Deleta IDs
query1 = """DELETE FROM bq_bexs_sandbox.pay_payments_prep WHERE ID IS NOT NULL"""
query_job1 = bq1.query(query1)

def insert_bigquery(target_uri, dataset_id, table_id):
    bigquery_client = bigquery.Client(credentials=credentials, project=credentials.project_id)
    dataset_ref = bigquery_client.dataset(dataset_id)
    job_config = bigquery.LoadJobConfig()
    job_config.schema = [
        bigquery.SchemaField('id','STRING',mode='REQUIRED')
    ]
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.field_delimiter = ";"
    uri = target_uri
    load_job = bigquery_client.load_table_from_uri(
        uri,
        dataset_ref.table(table_id),
        job_config=job_config
        )
    print('Starting job {}'.format(load_job.job_id))
    load_job.result()
    print('Job finished.')

insert_bigquery(bucket_name, dataset, tabela)

def get_data_from_bigquery():
    """query bigquery to get data to import to PSQL"""
    bq = bigquery.Client(credentials=credentials, project=credentials.project_id)
    #Busca IDs
    query = """SELECT id FROM bq_bexs_sandbox.pay_payments_prep"""
    query_job = bq.query(query)
    data = query_job.result()
    rows = list(data)
    return rows

a = get_data_from_bigquery()
length = len(a)
line_count = 0
schema = avro.schema.Parse(open(file_schema, "rb").read())  # need to know the schema to write. According to 1.8.2 of Apache Avro
writer = DataFileWriter(open(new_file, "wb"), avro.io.DatumWriter(), schema)

for row in range(length):
    bytes = base64.b64decode(str(a[row][0]))
    bytes = bytes[5:]
    buf = io.BytesIO(bytes)
    decoder = avro.io.BinaryDecoder(buf)
    rec_reader = avro.io.DatumReader(avro.schema.Parse(open(file_schema).read()))
    out=rec_reader.read(decoder)
    writer.append(out)
writer.close()

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    storage_client = storage.Client.from_service_account_json('cert/key.json')
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob("insert_payments/" + destination_blob_name)
    blob.upload_from_filename(source_file_name)
    print('File {} uploaded to {}'.format(
        source_file_name,
        destination_blob_name
    ))


upload_blob('bexspay_prd', new_file, new_filename)

def insert_bigquery_avro(target_uri, dataset_id, table_id):
    bigquery_client = bigquery.Client(credentials=credentials, project=credentials.project_id)
    dataset_ref = bigquery_client.dataset(dataset_id)
    job_config = bigquery.LoadJobConfig()
    job_config.autodetect = True
    job_config.source_format = bigquery.SourceFormat.AVRO
    job_config.use_avro_logical_types = True
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
    time_partitioning = bigquery.table.TimePartitioning()
#    time_partitioning = bigquery.table.TimePartitioning(type_=bigquery.TimePartitioningType.DAY, field="date")
    job_config.time_partitioning = time_partitioning
    job_config.clustering_fields = ["status", "type"]
    uri = target_uri
    load_job = bigquery_client.load_table_from_uri(
        uri,
        dataset_ref.table(table_id),
        job_config=job_config
        )
    print('Starting job {}'.format(load_job.job_id))
    load_job.result()
    print('Job finished.')

dataset1 = 'bexs_pay'
tabela1 = 'sale'
bucket_name1 = 'gs://bexspay_prd/insert_payments/' + new_filename


insert_bigquery_avro(bucket_name1, dataset1, tabela1)

if os.path.exists(new_file):
    os.remove(new_file)
    print("Delete file", new_file)
else:
    print("Can not delete the file as it doesn't exists")