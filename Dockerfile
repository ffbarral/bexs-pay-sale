FROM python:3.7

ADD *.py /app/
ADD requirements.txt /app/
ADD cert /app/cert
RUN pip install cython
RUN pip install CPython
RUN pip install -r /app/requirements.txt

WORKDIR /app

ENTRYPOINT [ "python", "pay_sale.py" ]